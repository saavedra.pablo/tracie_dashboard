# Copyright (c) 2019 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

from django import template

register = template.Library()

@register.filter
def status_icon(status):
    status_attrs = {
        'pass': ('pass', 'black', 'lawngreen','&#10003;'),
        'fail': ('fail', 'black', 'red','&#10007;'),
        'unknown': ('unknown', 'black', 'lightgray', '?'),
        }

    attr = status_attrs.get(status, ('invalid', 'white', 'white', ''))
    span = '<span class="%s" style="display: inline-block; width: 25px; height: 25px; border-radius: 25px; font-size: 125%%; color: %s; background-color: %s; text-align: center">%s</span>' % attr

    return span
