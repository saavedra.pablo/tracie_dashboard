# Copyright (c) 2019 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

"""A cache for CI results.

Fetching CI data from gitlab is typically slow, and most request require the
same subset of most recent data. It is therefore beneficial to store the data
in a local cache. This module provides such a cache for the CI results in the
form of a database using Django models.
"""

from dashboard.models import JobResult, TraceResult, PipelineResult, PerDeviceResults
import util.ci as ci
from concurrent.futures import Future, ThreadPoolExecutor
import asyncio

from django.db import transaction

fetch_executor = ThreadPoolExecutor()

def _mesa_gitlab_url(repo, commit_id):
    """Gets the gitlab URL for a Mesa commit id."""
    if commit_id is None:
        return None
    else:
        return repo.rstrip("/") + "/commit/" + commit_id

def _trace_result_from_ci(job, name):
    """Creates a TraceResult object from information returned by CI.

    Processes and translates the information returned by CI to create a
    TraceResult object.

    Args:
        job: The ci.Job this traces result is part of.
        name: The trace name.

    Returns:
        A PipelineResult object.
    """
    result = job.trace_results.get(name, "")

    if result == "match":
        tr_status = "pass"
    elif result == "differ":
        tr_status = "fail"
    else:
        tr_status = "unknown"

    return TraceResult(
            summary_url = job.trace_summary_url(name),
            status = tr_status,
            name = name,
            job_result_id = job.id)

# A map from CI status strings to PipelineResult status strings.
_ci_to_result_status_map = {
  "success" : "pass",
  "failed" : "fail",
  "pending" : "inprogress",
  "running" : "inprogress",
}

def _job_result_from_ci(job):
    """Creates a JobResult object from the information returned by CI.

    Processes and translates the information returned by CI to create a
    JobResult object.

    Args:
        job: a ci.Job object.

    Returns:
        A JobResult object.
    """

    return JobResult(
            id = job.id,
            project_path = job.project_path,
            url = job.url(),
            status = _ci_to_result_status_map.get(job.status, "unknown"),
            device = job.device,
            timestamp = job.timestamp if job.timestamp is not None else "0000-00-00T00:00:00.000Z",
            pipeline_result_id = job.pipeline_id)

def _pipeline_result_from_ci(pipeline):
    """Creates a PipelineResult object from the information returned by CI.

    Processes and translates the information returned by CI to create a
    PipelineResult object.

    Args:
        pipeline: a ci.Pipeline object.

    Returns:
        A PipelineResult object.
    """

    return PipelineResult(
            id = pipeline.id,
            project_path = pipeline.project_path,
            url = pipeline.url(),
            mesa_sha = pipeline.mesa_sha,
            mesa_url = _mesa_gitlab_url(pipeline.mesa_repo, pipeline.mesa_sha),
            status = _ci_to_result_status_map.get(pipeline.status, "unknown"))

async def pipeline_result_for_id(project_path, pid):
    """Gets the PipelineResult object for a particular pipeline id.

    If the PipelineResult object is not present in the cache, this
    function contacts the CI server to fetch all required information
    for the pipeline id, and creates a new cache entry for it.

    This function is asynchronous and returns a future containing
    the PipelineResult.

    Args:
        project_path: The project_path.
        pid: The pipeline id.

    Returns:
        A concurrency.futures.Future object containing the PipelineResult
        object for the requested pipeline.
    """
    db_pr = PipelineResult.objects.filter(id=pid)
    if db_pr:
        assert(len(db_pr) == 1)
        # Only return completed pipelines from the cache
        if db_pr[0].status != "inprogress":
            return db_pr[0]

    def ci_fetch_func():
        pipeline = ci.get_pipeline(project_path, pid)
        test_jobs = [ci.get_test_job(project_path, jid) for jid in pipeline.test_job_ids]
        return pipeline, test_jobs

    loop = asyncio.get_running_loop()
    pipeline, test_jobs = \
        await loop.run_in_executor(fetch_executor, ci_fetch_func)

    # Create and store the PipelineResult and all associated JobResult
    # TraceResult objects.
    presult = _pipeline_result_from_ci(pipeline)
    job_results = []
    trace_results = []
    for test_job in test_jobs:
        job_results.append(_job_result_from_ci(test_job))
        trace_results.extend(
            [_trace_result_from_ci(test_job, t)
             for t in test_job.trace_results.keys()])

    with transaction.atomic():
        presult.save()
        for jresult in job_results:
            TraceResult.objects.filter(job_result_id=jresult.id).delete()
            jresult.save()
        for tresult in trace_results:
            tresult.save()

    return presult

async def job_result_for_id(project_path, jid):
    """Gets a JobResult object for a particular job id.

    If the JobResult objects is not present in the cache, this
    function contacts the CI server to fetch all required information
    for the job id, and creates a new cache entry for it.

    This function is asynchronous and returns a future containing
    the JobResult.

    Args:
        project_path: The project path.
        jid: The job id.

    Returns:
        A concurrency.futures.Future object containing the
        JobResult object for the requested job.
    """
    db_jr = JobResult.objects.filter(id=jid)
    if db_jr:
        assert(len(db_jr) == 1)
        # Only return completed jobs from the cache
        if db_jr[0].status != "inprogress":
            return db_jr[0]

    def ci_fetch_func():
        return ci.get_test_job(project_path, jid)

    loop = asyncio.get_running_loop()

    test_job = await loop.run_in_executor(fetch_executor, ci_fetch_func)
    await pipeline_result_for_id(project_path, test_job.pipeline_id)

    return JobResult.objects.get(id=jid)

def _init_device_info(id, job):
    device_info = PerDeviceResults()
    device_info.id = id
    device_info.name = job.device

    device_info.last_run = job
    if job.status == "pass":
        device_info.last_good_run = job
    else:
        device_info.first_bad_run = job
    return device_info

def _update_device_info(device_info, job):
    if device_info.last_run.timestamp >= job.timestamp:
        return False

    device_info.last_run = job
    if job.status == "pass":
        device_info.last_good_run = job
        device_info.first_bad_run = None
    else:
        device_info.first_bad_run = job
    return True

def all_device_results(project_path, pipeline_results):
    for p in pipeline_results:
        for j in p.job_results.all():
            id = project_path + ":" + j.device
            update = False
            qdevice_result = PerDeviceResults.objects.filter(id=id)
            device_result = None
            if qdevice_result:
                device_result = qdevice_result[0]
                update = _update_device_info(device_result, j)
            else:
                device_result = _init_device_info(id, j)
                update = True
            if update:
                device_result.save()

    return PerDeviceResults.objects.filter(id__startswith=project_path + ":")
